package com.basildsouza.games.parker.engine.test;

import java.io.IOException;
import java.util.List;

import com.basildsouza.games.parker.engine.Room;
import com.basildsouza.games.parker.engine.RoomManager;

public class RoomTester_4x4_12P_12I_Run2 extends GenericRoomTester{
	public static void main(String args[]) throws IOException{
		String inputFileLoc = "C:\\Personal\\Projects\\InspectorParker\\Sample Test Cases\\4x4-12P-12I-Run2\\UnitInput.txt";
		String outputFileLoc = "C:\\Personal\\Projects\\InspectorParker\\Sample Test Cases\\4x4-12P-12I-Run2\\UnitOutput.txt";
		
		List<Room[][]> roomList = new RoomTester_4x4_12P_12I_Run2().compareRooms(inputFileLoc, outputFileLoc);
		
		Room[][] solvedRoom = roomList.get(0);
		Room[][] outputRoom = roomList.get(1);
		
		System.out.println("Solved Room:");
		RoomManager.printRooms(solvedRoom);
		System.out.println("Output Room:");
		RoomManager.printRooms(outputRoom);
	}
}
