package com.basildsouza.games.parker.engine.test;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.basildsouza.games.parker.cui.ClueReader;
import com.basildsouza.games.parker.cui.RoomReader;
import com.basildsouza.games.parker.engine.Clue;
import com.basildsouza.games.parker.engine.Room;
import com.basildsouza.games.parker.engine.RoomManager;


public class GenericRoomTester {
	
	public List<Room[][]> compareRooms(String inputFileLoc, String outputFileLoc) throws IOException{
		
		BufferedReader reader = new BufferedReader(new FileReader(inputFileLoc));
		RoomReader roomReader = new RoomReader(reader);
		Room[][] rooms = roomReader.readRooms();
		
		ClueReader clueReader = new ClueReader();
		List<Clue> clues = clueReader.readClues(reader);
		
		reader.close();
		
		reader = new BufferedReader(new FileReader(outputFileLoc));
		roomReader = new RoomReader(reader);
		Room[][] outputRooms = roomReader.readRooms();
		Room[][] solvedRooms = RoomManager.solveCrime(rooms, clues);
		
		if(!assertRoomsEqual(solvedRooms, outputRooms)){
			System.out.println("Rooms not equal.");
		}
		
		List<Room[][]> roomList = new ArrayList<Room[][]>(2);
		
		roomList.add(solvedRooms);
		roomList.add(outputRooms);
		
		return roomList;
	}

	private static boolean assertRoomsEqual(Room[][] rooms, Room[][] outputRooms) {
		if(rooms.length != outputRooms.length){
			return false;
		}
		if(rooms[0].length != outputRooms[0].length){
			return false;
		}
		
		for(int i=0; i<rooms.length; i++){
			for(int j=0; j<rooms[i].length; j++){
				if(!rooms[i][j].getMembers().equals(outputRooms[i][j].getMembers())){
					return false;
				}
			}
		}
		return true;
	}
	
}
