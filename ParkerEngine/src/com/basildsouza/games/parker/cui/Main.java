package com.basildsouza.games.parker.cui;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;

import com.basildsouza.games.parker.engine.Clue;
import com.basildsouza.games.parker.engine.Room;
import com.basildsouza.games.parker.engine.RoomManager;

public class Main {
	public static void main(String[] args) throws NumberFormatException, IOException{
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		RoomReader roomReader = new RoomReader(reader);
		ClueReader clueReader = new ClueReader();
		
		Room[][] rooms = roomReader.readRooms();
		List<Clue> clues = clueReader.readClues(reader);
		Room[][] solvedRooms = RoomManager.solveCrime(rooms, clues);
		RoomManager.printRooms(solvedRooms);
	}
	
}
