package com.basildsouza.games.parker.cui;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.basildsouza.games.parker.engine.Evidence;

public class EvidenceReader {
	private static final String EVIDENCE_FORMAT = "%-20s %-20s %-20s";
	
	public void printEvidenceList(boolean isPerson){
		List<Evidence> evidence = Evidence.getEvidence(isPerson);
		System.out.println("List of " + (isPerson?"People":"Items") + ": ");
		for(int i=0; i<evidence.size(); i+=3){
			String evidence1 = "";
			String evidence2 = "";
			String evidence3 = "";

			evidence1 = i+0 + ". " + evidence.get(i).getName();
			if(i+1 < evidence.size()){
				evidence2 = i+1 + ". " + evidence.get(i+1).getName();
				if(i+2 < evidence.size()){
					evidence3 = i+2 + ". " + evidence.get(i+2).getName();
				}
			}
			System.out.printf(EVIDENCE_FORMAT, evidence1, evidence2, evidence3);
			System.out.println();
		}
		System.out.println();
	}
	
	public Set<Evidence> readEvidenceList(BufferedReader reader, boolean isPerson) throws IOException{
		System.out.print((isPerson?"People":"Items") + " (csv): ");
		String csvEvidence = reader.readLine().trim();
		
		if(csvEvidence.length() == 0){
			return Collections.emptySet();
		}

		String[] evidenceIndexes = csvEvidence.split("[ ]*\\,[ ]*");
		Set<Evidence> evidenceSet = new HashSet<Evidence>(evidenceIndexes.length);
		List<Evidence> evidenceList = Evidence.getEvidence(isPerson);

		for(String index : evidenceIndexes){
			evidenceSet.add(evidenceList.get(Integer.parseInt(index.trim())));
		}
		
		return evidenceSet;
	}
}
