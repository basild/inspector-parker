package com.basildsouza.games.parker.cui;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.Set;

import com.basildsouza.games.parker.engine.Evidence;
import com.basildsouza.games.parker.engine.Room;

public class RoomReader {
	private final BufferedReader reader;
	
	public RoomReader(BufferedReader reader){
		this.reader = reader;
	}
	
	public Room[][] readRooms() throws NumberFormatException, IOException{
		Room[][] rooms;
		int x, y;
		
		System.out.println("Room X Num: ");
		x = Integer.parseInt(reader.readLine());
		System.out.println("Room Y Num: ");
		y = Integer.parseInt(reader.readLine());
		
		rooms = new Room[x][y];
		EvidenceReader evidenceReader = new EvidenceReader();
		evidenceReader.printEvidenceList(true);
		evidenceReader.printEvidenceList(false);
		
		for(int i=0; i<x; i++){
			for(int j=0; j<y;j++){
				System.out.println("For Room (" + i + ", " + j + "): ");
				Set<Evidence> personSet = evidenceReader.readEvidenceList(reader, true);
				Set<Evidence> itemSet = evidenceReader.readEvidenceList(reader, false);
				
				rooms[i][j] = new Room(i, j, personSet, itemSet);
			}
		}
		
		return rooms;
	}
}
