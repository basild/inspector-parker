package com.basildsouza.games.parker.cui;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import com.basildsouza.games.parker.engine.Clue;
import com.basildsouza.games.parker.engine.CluePart;
import com.basildsouza.games.parker.engine.Direction;
import com.basildsouza.games.parker.engine.Evidence;
import com.basildsouza.games.parker.engine.Displacement;

public class ClueReader {
	private List<Clue> readClue(BufferedReader reader) throws IOException{
		System.out.println("Name ([PI]Index): ");
		String input = reader.readLine().trim();
		if(input == null || input.length() == 0){
			return null;
		}
		
		Evidence evidence = parseEvidence(input);
		System.out.println("Direction ([?0-9][NESW]): ");
		input = reader.readLine().trim();
		List<CluePart> cluePartList = new LinkedList<CluePart>();
		
		while(input != null && input.length() > 0){
			Displacement transform = parseTransform(input);
			System.out.println("Name ([PI]Index): ");
			cluePartList.add(new CluePart(evidence, transform));
			
			input = reader.readLine().trim();
			evidence = parseEvidence(input);
			
			System.out.println("Direction ([?0-9][NESW]): ");
			input = reader.readLine().trim();
		}
		cluePartList.add(new CluePart(evidence, null));
		
		return CluePart.splitClues(cluePartList);
	}
	
	public List<Clue> readClues(BufferedReader reader) throws IOException{
		System.out.println("Enter Clues:");
		System.out.println("	Empty Name to stop entering clues");
		System.out.println("	Empty Transform to stop current clue");
		List<Clue> clueList = new LinkedList<Clue>();
		List<Clue> clue = readClue(reader);
		List<List<Clue>> unbrokenClueList = new LinkedList<List<Clue>>();
		while(clue != null){
			clueList.addAll(clue);
			unbrokenClueList.add(clue);
			clue = readClue(reader);
		}
		
		printOutIndividualClues(unbrokenClueList);
		return clueList;
	}
	private static Evidence parseEvidence(String input){
		boolean isPerson = input.charAt(0) == 'P';
		int index = Integer.parseInt(input.substring(1));
		
		return Evidence.getEvidence(isPerson).get(index);
	}
	
	private static Displacement parseTransform(String input){
		int distance;
		if(input.charAt(0) == '?'){
			distance = -1;
		} else {
			distance = Integer.parseInt(input.substring(0, 1));
		}
		Direction dir;
		switch(Character.toUpperCase(input.charAt(1))){
			case 'N':
				dir = Direction.NORTH;
				break;
			case 'S':
				dir = Direction.SOUTH;
				break;
			case 'E':
				dir = Direction.EAST;
				break;
			case 'W':
				dir = Direction.WEST;
				break;
			default:
				throw new IllegalArgumentException("Invalid Identifier: " + input.charAt(1));
		}
		
		return new Displacement(dir, distance);
	}
	
	private static void printOutIndividualClues(List<List<Clue>> unbrokenClueList){
		for(List<Clue> clue : unbrokenClueList){
			for(Clue cluePart : clue){
				System.out.print(cluePart.toString() + ". ");
			}
			System.out.println();
		}
	}
}
