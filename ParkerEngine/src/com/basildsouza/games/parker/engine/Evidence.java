package com.basildsouza.games.parker.engine;

import java.util.LinkedList;
import java.util.List;

public enum Evidence {
	// Person:
	FRED("Fred", "Fred", true),
	MAURICE("Maurice", "Maurice", true),
	BORRIS("Borris", "Borris", true),
	OPHELIA("Ophelia", "Ophelia", true),
	PORTIA("Portia", "Portia", true),
	HARRIET("Harriet", "Harriet", true),
	CORDELIA("Cordelia", "Cordelia", true),
	NANCY("Nancy", "Nancy", true),
	ROLLY("Rolly", "Rolly", true),
	STELLA("Stella", "Stella", true),
	GRANPA_GUS("Grandpa Gus", "Grandpa_Gus", true),
	LAWRENCE("Lawrence", "Lawrence", true),
	
	// Items:
	LAUDANUM("Laudanum", "Laudanum", false),
	KNIFE("Knife", "Knife", false),
	NOOSE("Noose", "Noose", false),
	AXE("Axe", "Axe", false),
	GUN("Gun", "Gun", false),
	RAT_POISON("Rat Poison", "Rat_Poison", false),
	FURIOUS_LETTER("Furious Letter", "Furious_Letter", false),
	LAST_WILL("Last Will", "Last_Will", false),
	MONEY("Money", "Money", false),
	BABY_PROOF("Baby Proof", "Baby_Proof", false),
	LOVE_LETTER("Love Letter", "Love_Letter", false),
	JEWELLERY_BOX("Jewellery Box", "Jewellery_Box", false);
	
	
	Evidence(String name, String sprite, boolean person){
		this.name = name;
		this.sprite = sprite;
		this.person = person;
	}

	final String name;
	final String sprite;
	final boolean person;
	
	public String getName() {
		return name;
	}
	
	public boolean isPerson() {
		return person;
	}
	
	public String getSprite() {
		return sprite;
	}
	
	private static List<Evidence> personList;
	private static List<Evidence> itemList;
	
	public static List<Evidence> getEvidence(boolean isPerson){
		if(personList == null || itemList == null){
			segregateEvidence();
		}
		
		return isPerson?personList:itemList;
	}
	
	public static void segregateEvidence(){
		Evidence[] allEvidence = values();
		personList = new LinkedList<Evidence>();
		itemList = new LinkedList<Evidence>();
		for(Evidence evidence : allEvidence){
			if(evidence.isPerson()){
				personList.add(evidence);
			} else {
				itemList.add(evidence);
			}
		}
	}
	
	@Override
	public String toString() {
		return name;
	}
}
