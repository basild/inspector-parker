package com.basildsouza.games.parker.engine;

import java.util.LinkedList;
import java.util.List;

public class CluePart {
	private final Evidence evidence;
	private final Displacement transform;
	
	public CluePart(Evidence evidence, Displacement transform) {
		this.evidence = evidence;
		this.transform = transform;
	}
	
	public Evidence getEvidence() {
		return evidence;
	}
	
	public Displacement getTransform() {
		return transform;
	}
	
	public static List<Clue> splitClues(List<CluePart> clueParts){
		List<Clue> clueList = new LinkedList<Clue>();
		
		for(int i=0; i<clueParts.size()-1; i++){
			CluePart currClue = clueParts.get(i);
			CluePart nextClue = clueParts.get(i+1);
			Clue clue = new Clue(currClue.getEvidence(), 
					  			 currClue.getTransform(), 
					  			 nextClue.getEvidence());
			clueList.add(clue);
			clueList.add(clue.reverseClue());
		}
		
		return clueList;
	}
	
	public static String toString(List<CluePart> clueParts){
		StringBuilder sb = new StringBuilder(40);
		
		int i;
		for(i=0; i<clueParts.size()-1; i++){
			CluePart currClue = clueParts.get(i);
			sb.append(currClue.getEvidence()).append(" ")
			  .append(currClue.getTransform().toDescriptiveString()).append(" ");
		}
		sb.append(clueParts.get(i).getEvidence());
		
		return sb.toString();
	}
}
