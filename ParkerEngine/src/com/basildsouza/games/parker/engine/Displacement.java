package com.basildsouza.games.parker.engine;

public class Displacement {
	private final Direction direction;
	private final int distance;
	
	public Displacement(Direction direction, int distance) {
		this.direction = direction;
		this.distance = distance;
	}
	
	public Direction getDirection() {
		return direction;
	}
	
	public int getDistance() {
		return distance;
	}
	
	@Override
	public String toString() {
		return ((distance==-1)?"?":Integer.toString(distance)) + direction.toString().charAt(0);
	}
	
	public String toDescriptiveString(){
		if(distance == -1){
			return "?" +  direction.toString().charAt(0);
		}
		if(distance == 0){
			return "+";
		}
		return Integer.toString(distance) + direction.toString().charAt(0);
	}

	public Displacement reverseDisplacement() {
		return new Displacement(getOppositeDirection(direction), distance);
	}
	
	private Direction getOppositeDirection(Direction d){
		if(d == Direction.NORTH){
			return Direction.SOUTH;
		} else if (d == Direction.SOUTH){
			return Direction.NORTH;
		} else if (d == Direction.WEST){
			return Direction.EAST;
		} else if ( d == Direction.EAST){
			return Direction.WEST;
		}
		
		throw new IllegalArgumentException("Unable to find opposite direction.");
	}
}
