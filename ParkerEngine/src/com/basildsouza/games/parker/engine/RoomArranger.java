package com.basildsouza.games.parker.engine;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

//TODO: Find the proper default values to put in rooms
// Make a room arranger, with different models (3x2, 3x3, 4x3, 3x4, 4x4, default)
// Default will just return empty lists, others will give you necessary inputs

//TODO: Add a new UI element that also includes a difficulty (to automatically prefill the room)
public class RoomArranger {
	public static RoomLayout getRoomLayout(int maxX, int maxY){
		if(maxX == 4 && maxY == 4){
			return new RoomLayout() {
				List<Evidence>[] upperRoom 
						= new List[]{
							Arrays.asList(new Evidence[]{
									Evidence.CORDELIA,
									Evidence.NANCY,
									Evidence.ROLLY,
									Evidence.STELLA,
									Evidence.GRANPA_GUS,
									Evidence.LAWRENCE}), 
							Arrays.asList(new Evidence[]{
									Evidence.FURIOUS_LETTER,
									Evidence.LAST_WILL,
									Evidence.MONEY,
									Evidence.BABY_PROOF,
									Evidence.LOVE_LETTER,
									Evidence.JEWELLERY_BOX	})};
				List<Evidence>[] lowerRoom
						= new List[]{
						Arrays.asList(new Evidence[]{
								Evidence.FRED,
								Evidence.MAURICE,
								Evidence.BORRIS,
								Evidence.OPHELIA,
								Evidence.PORTIA,
								Evidence.HARRIET}), 
						Arrays.asList(new Evidence[]{
								Evidence.LAUDANUM,
								Evidence.KNIFE,
								Evidence.NOOSE,
								Evidence.AXE,
								Evidence.GUN,
								Evidence.RAT_POISON	})};

				public Set<Evidence>[] getEvidence(int roomX, int roomY) {
					if(roomY > 1){
						return new Set[]{new HashSet<Evidence>(lowerRoom[0]), 
										  new HashSet<Evidence>(lowerRoom[1])};
					} else {
						return new Set[]{new HashSet<Evidence>(upperRoom[0]), 
								  new HashSet<Evidence>(upperRoom[1])};
					}
				}
			};
		}
		
		return new RoomLayout() {
			
			public Set<Evidence>[] getEvidence(int roomX, int roomY) {
				return new Set[]{new HashSet<Evidence>(), new HashSet<Evidence>()};
			}
		};
	}
	public interface RoomLayout{
		public Set<Evidence>[] getEvidence(int roomX, int roomY);
	}
}
