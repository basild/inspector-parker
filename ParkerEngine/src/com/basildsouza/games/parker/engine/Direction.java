package com.basildsouza.games.parker.engine;

public enum Direction {
	NORTH,
	EAST,
	SOUTH,
	WEST;
}
