package com.basildsouza.games.parker.engine;

import java.util.HashSet;
import java.util.Set;

public class Room {
	private final int xPos;
	private final int yPos;
	private final Set<Evidence> itemSet;
	private final Set<Evidence> personSet;
	
	private Evidence selectedItem, selectedPerson;
	private Room[] neighbours;
	
	public Room(int xPos, int yPos, Set<Evidence> personSet, Set<Evidence> itemSet){
		this.xPos = xPos;
		this.yPos = yPos;
		this.personSet = personSet;
		this.itemSet = itemSet;
		
	}
	
	public void setNeighbours(Room[] neighbours){
		this.neighbours = neighbours;
	}
	
	public boolean containsEvidence(Evidence evidence){
		return evidence.isPerson()?
					personSet.contains(evidence):
					itemSet.contains(evidence);
	}
	
	public int getXPos() {
		return xPos;
	}
	
	public int getYPos() {
		return yPos;
	}
	
	public boolean wouldRemove(Evidence evidence){
		return evidence.isPerson()?personSet.contains(evidence):itemSet.contains(evidence);
	}
	public boolean removeEvidence(Evidence evidence){
		if(evidence.isPerson()){
			if(selectedPerson == evidence){
				RoomManager.printCurrRooms();
				throw new IllegalStateException("Room (" + getXPos() + ", " + getYPos() + 
						   ") Removing a selected person: " + evidence);
			}
		} else {
			if (selectedItem == evidence){
				RoomManager.printCurrRooms();
				throw new IllegalStateException("Room (" + getXPos() + ", " + getYPos() + 
						   ") Removing a selected Item: " + evidence);
			}
		}
		Set<Evidence> evidenceSet = evidence.isPerson()?personSet:itemSet;
		if(!evidenceSet.contains(evidence)){
			return false;
		}
		
		evidenceSet.remove(evidence);
		if(!RoomManager.removeEvidence(this, evidence)){
			System.err.println("Not removed!!!");
		}
		
		return true;
	}
	
	// The only person who should call this is the room manager
	public void selectEvidence(Evidence evidence){
		Set<Evidence> evidenceSet = evidence.isPerson()?personSet:itemSet;
		
		//First Removing it:
		evidenceSet.remove(evidence);
		
		// Removing all properly:
		Set<Evidence> tempSet = new HashSet<Evidence>(evidenceSet);
		for(Evidence remEv : tempSet){
			removeEvidence(remEv);
		}
		
		// Then Readding only the selected one:
		evidenceSet.add(evidence);
		if(evidence.isPerson()){
			selectedPerson = evidence;
		} else {
			selectedItem = evidence;
		}
		RoomManager.handleSelection(this, evidence);
		RoomManager.notifyChange();
	}
	
	public Set<Evidence> getItemSet() {
		return itemSet;
	}
	
	public Set<Evidence> getPersonSet() {
		return personSet;
	}
	
	public Room getNeighbour(Direction dir, int steps){
		if(steps == 0){
			return this;
		}
		Room immNeighbour = neighbours[dir.ordinal()];
		if(immNeighbour == null){
			return null;
		}
		return immNeighbour.getNeighbour(dir, steps-1);
	}
	
	@Override
	public String toString() {
		return "(" + xPos + ", " + yPos + ")";
	}
	
	public String getMembers(){
		return "----------\n" + 
		   personSet.toString() + "\n" + 
		   itemSet.toString() + "\n" + 
		   "----------\n";
		
	}
}
