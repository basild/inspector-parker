package com.basildsouza.games.parker.engine;


public class Clue {
	private final Evidence[] evidence;
	private final Displacement transform;
	
	public Clue(Evidence start, Displacement transform, Evidence end){
		evidence = new Evidence[]{start, end};
		this.transform = transform;
	}
	
	public void evaluate(Room room){
		if(transform.getDistance() < 0){
			evaluateUnknownDistance(room);
		} else {
			evaluateKnownDistance(room);
		}
	}
	
	public void evaluateKnownDistance(Room room){
		if(!room.containsEvidence(evidence[0])){
			return; // No Change
		}
		Room neighbour = room.getNeighbour(transform.getDirection(), transform.getDistance());
		if(neighbour == null || !neighbour.containsEvidence(evidence[1])){
			RoomManager.logStep(room, "Eliminating: " + evidence[0] + 
					" Using Clue: " + toString());
			room.removeEvidence(evidence[0]);
		}
	}
	public void evaluateUnknownDistance(Room room){
		if(!room.containsEvidence(evidence[0])){
			return; // No Change
		}
		
		Room neighbour = room.getNeighbour(transform.getDirection(), 1);
		
		while (neighbour != null) {
			if(neighbour.containsEvidence(evidence[1])){
				return; // Found
			}

			neighbour = neighbour.getNeighbour(transform.getDirection(), 1);
		}
		RoomManager.logStep(room, "Eliminating: " + evidence[0] + 
							" Using Clue: " + toString());
		
		room.removeEvidence(evidence[0]);
	}
	
	@Override
	public String toString() {
		return evidence[0] + "-" + transform.toString() + "-" + evidence[1]; 
	}
	
	public Clue reverseClue(){
		return new Clue(evidence[1], transform.reverseDisplacement(), evidence[0]);
	}
	
	
}
