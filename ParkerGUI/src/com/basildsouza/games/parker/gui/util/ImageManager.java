package com.basildsouza.games.parker.gui.util;

import javax.swing.ImageIcon;

public class ImageManager {
	//TODO: Cache this later
	public static ImageIcon get16x16Image(String sprite){
		return getImage(sprite, 16);
	}

	public static ImageIcon get24x24Image(String sprite){
		return getImage(sprite, 24);
	}
	
	public static ImageIcon get32x32Image(String sprite){
		return getImage(sprite, 32);
	}

	public static ImageIcon get48x48Image(String sprite){
		return getImage(sprite, 48);
	}
	
	public static ImageIcon getImage(String sprite, int size){
		return new ImageIcon(ImageManager.class.getResource("/" + size + "x" + size + "/" + sprite + ".png"));
	}

}
