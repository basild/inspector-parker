package com.basildsouza.games.parker.gui.fragments;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.UIManager;
import javax.swing.border.TitledBorder;

import com.basildsouza.games.parker.engine.Clue;
import com.basildsouza.games.parker.engine.CluePart;
import com.basildsouza.games.parker.engine.Displacement;
import com.basildsouza.games.parker.engine.Evidence;
import com.basildsouza.games.parker.gui.util.ImageManager;

public class ClueBuilder extends JDialog {

	private static final long serialVersionUID = 1L;
	private final JPanel contentPanel = new JPanel();

	private final EvidenceDialog evidenceDialog = new EvidenceDialog();
	private JButton addBtn;
	private JTextArea evidenceTextArea;
	private EvidenceListener[] evdListeners = new EvidenceListener[3];
	private DisplacementListener[] disListeners = new DisplacementListener[2];

	// These shouldve been in a model class:
	private final Evidence[] evdArr = new Evidence[3];
	private final Displacement[] dirArr = new Displacement[2];
	
	private List<CluePart> newCluePartList;
	private List<List<CluePart>> addedCluePartList = new LinkedList<List<CluePart>>();
	private DefaultComboBoxModel addedClueModel = new DefaultComboBoxModel();
	
	private List<Clue> finalSplitClues = new LinkedList<Clue>();

	/**
	 * Create the dialog.
	 */
	public ClueBuilder() {
		setResizable(false);
		setTitle("Clue Builder");
		setBounds(100, 100, 405, 301);
		setModal(true);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(UIManager.getBorder("MenuBar.border"));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));

		JPanel newCluePanel = createNewCluePanel();
		contentPanel.add(newCluePanel);

		JPanel modifyPanel = createModifyPanel();
		contentPanel.add(modifyPanel);		
		
		JPanel buttonPanel = createButtonPanel();
		getContentPane().add(buttonPanel, BorderLayout.SOUTH);

		setDefaultCloseOperation(HIDE_ON_CLOSE);
		validateEntries();
	}
	
	private JPanel createModifyPanel(){
		JPanel modifyPanel = new JPanel();
		modifyPanel.setBorder(new TitledBorder(null, "Modify Clues", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		modifyPanel.setPreferredSize(new Dimension(getBounds().width-10, 90));
		
		JComboBox addComboBox = new JComboBox();
		addComboBox.setPreferredSize(new Dimension(300, addComboBox.getPreferredSize().height));
		addComboBox.setModel(addedClueModel);
		modifyPanel.add(addComboBox);
		
		JButton removeBtn = new JButton("Remove");
		removeBtn.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				Object selectedItem = addedClueModel.getSelectedItem();
				if(selectedItem == null){
					return;
				}
				
				int index = addedClueModel.getIndexOf(selectedItem);
				addedClueModel.removeElementAt(index);
				addedCluePartList.remove(index);
			}
		});
		modifyPanel.add(removeBtn);
		
		return modifyPanel;
	}
	
	private JPanel createButtonPanel(){
		JPanel buttonPanel = new JPanel();
		buttonPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));

		JButton okButton = new JButton("OK");
		okButton.setActionCommand("OK");
		okButton.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				finalSplitClues = new LinkedList<Clue>();
				for(List<CluePart> unSplitClue : addedCluePartList){
					List<Clue> splitClues = CluePart.splitClues(unSplitClue);
					finalSplitClues.addAll(splitClues);
				}
				setVisible(false);
			}
		});
		buttonPanel.add(okButton);
		getRootPane().setDefaultButton(okButton);

		JButton cancelButton = new JButton("Cancel");
		cancelButton.setActionCommand("Cancel");
		cancelButton.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				finalSplitClues = new LinkedList<Clue>();
				setVisible(false);
			}
		});
		buttonPanel.add(cancelButton);
	
		return buttonPanel;
	}
	
	private JPanel createNewCluePanel(){
		JPanel newCluePanel = new JPanel();
		newCluePanel.setBorder(new TitledBorder(null, "New Clue", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		newCluePanel.setPreferredSize(new Dimension(getBounds().width-10, 135));
		contentPanel.add(newCluePanel);
		GridBagLayout gbl_newCluePanel = new GridBagLayout();
		gbl_newCluePanel.columnWidths = new int[]{270, 0, 0};
		gbl_newCluePanel.rowHeights = new int[]{58, 0, 0};
		gbl_newCluePanel.columnWeights = new double[]{1.0, 0.0, Double.MIN_VALUE};
		gbl_newCluePanel.rowWeights = new double[]{0.0, 1.0, Double.MIN_VALUE};
		newCluePanel.setLayout(gbl_newCluePanel);

		JPanel popupPanel = createClueButtonPanel();
		GridBagConstraints gbc_popupPanel = new GridBagConstraints();
		gbc_popupPanel.fill = GridBagConstraints.HORIZONTAL;
		gbc_popupPanel.gridwidth = 3;
		gbc_popupPanel.insets = new Insets(0, 0, 5, 0);
		gbc_popupPanel.anchor = GridBagConstraints.NORTHWEST;
		gbc_popupPanel.gridx = 0;
		gbc_popupPanel.gridy = 0;
		newCluePanel.add(popupPanel, gbc_popupPanel);

		evidenceTextArea = new JTextArea();
		evidenceTextArea.setLineWrap(true);
		evidenceTextArea.setEditable(false);
		evidenceTextArea.setWrapStyleWord(true);
		evidenceTextArea.setText("Evidence Text will go here");
		GridBagConstraints gbc_evidenceTextArea = new GridBagConstraints();
		gbc_evidenceTextArea.insets = new Insets(0, 0, 0, 5);
		gbc_evidenceTextArea.fill = GridBagConstraints.BOTH;
		gbc_evidenceTextArea.gridx = 0;
		gbc_evidenceTextArea.gridy = 1;
		newCluePanel.add(evidenceTextArea, gbc_evidenceTextArea);

		addBtn = new JButton("Add");
		addBtn.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				addedClueModel.addElement(CluePart.toString(newCluePartList));
				addedCluePartList.add(newCluePartList);
				clearNewClueSelection();
			}
		});
		GridBagConstraints gbc_addBtn = new GridBagConstraints();
		gbc_addBtn.anchor = GridBagConstraints.EAST;
		gbc_addBtn.gridx = 1;
		gbc_addBtn.gridy = 1;
		newCluePanel.add(addBtn, gbc_addBtn);
		
		return newCluePanel;
	}
	
	private JPanel createClueButtonPanel(){
		JPanel popupPanel = new JPanel();
		FlowLayout fl_popupPanel = (FlowLayout) popupPanel.getLayout();
		fl_popupPanel.setHgap(15);

		JButton evdBtn1 = new JButton("E");
		popupPanel.add(evdBtn1);
		evdBtn1.setPreferredSize(new Dimension(48, 48));
		evdListeners[0] = new EvidenceListener(evdBtn1, 0);
		evdBtn1.addActionListener(evdListeners[0]);

		JButton dirBtn1 = new JButton("D");
		popupPanel.add(dirBtn1);
		dirBtn1.setPreferredSize(new Dimension(48, 48));
		disListeners[0] = new DisplacementListener(dirBtn1, 0);
		dirBtn1.addActionListener(disListeners[0]);

		JButton evdBtn2 = new JButton("E");
		popupPanel.add(evdBtn2);
		evdBtn2.setPreferredSize(new Dimension(48, 48));
		evdListeners[1] = new EvidenceListener(evdBtn2, 1);
		evdBtn2.addActionListener(evdListeners[1]);

		JButton dirBtn2 = new JButton("D");
		popupPanel.add(dirBtn2);
		dirBtn2.setPreferredSize(new Dimension(48, 48));
		disListeners[1] = new DisplacementListener(dirBtn2, 1);
		dirBtn2.addActionListener(disListeners[1]);

		JButton evdBtn3 = new JButton("E");
		popupPanel.add(evdBtn3);
		evdBtn3.setPreferredSize(new Dimension(48, 48));
		evdListeners[2] = new EvidenceListener(evdBtn3, 2);
		evdBtn3.addActionListener(evdListeners[2]);
		
		return popupPanel;
	}

	private class EvidenceListener implements ActionListener {
		private final JButton button;
		private final int num;
		
		public EvidenceListener(JButton button, int num){
			this.button = button;
			this.num = num;
		}
		
		public void actionPerformed(ActionEvent e) {
			Evidence evidence = evidenceDialog.selectEvidence();
			setEvidence(evidence);
			
		}
		
		public void setEvidence(Evidence evidence){
			if(evidence != null){
				button.setIcon(ImageManager.get48x48Image(evidence.getSprite()));
				button.setText("");
			} else {
				button.setIcon(null);
				button.setText("E");
			}
			
			setClues(evidence, num);
		}
	}
	
	private class DisplacementListener implements ActionListener {
		private final JButton button;
		private final int num;
		
		public DisplacementListener(JButton button, int num){
			this.button = button;
			this.num = num;
		}
		
		public void actionPerformed(ActionEvent e) {
			Displacement displacement = new DisplacementDialog().selectDisplacement();
			setDisplacement(displacement);
		}
		
		public void setDisplacement(Displacement displacement){
			if(displacement != null){
				button.setText(displacement.toString());
			} else {
				button.setText("D");
			}
			
			setClues(displacement, num);
		}
	}
	
	private void clearNewClueSelection(){
		for(int i=0; i<2; i++){
			evdListeners[i].setEvidence(null);
			disListeners[i].setDisplacement(null);
		}
		evdListeners[2].setEvidence(null);
		
	}
	
	private void setClues(Displacement dir, int num){
		dirArr[num] = dir;
		
		validateEntries();
	}
	private void setClues(Evidence evd, int num){
		evdArr[num] = evd;
		
		validateEntries();
	}

	private void updateClueText(boolean isValid){
		String text = "Select the Evidence and Displacement.";
		if(isValid){
			text = CluePart.toString(newCluePartList);
		}
		evidenceTextArea.setText(text);
	}
	private void validateEntries(){
		boolean isValid = buildClue();
		updateClueText(isValid);
		addBtn.setEnabled(isValid);
	}
	
	private boolean buildClue(){
		// Atleast 2 Evidence and 1 displacement (And others must be null)
		if(evdArr[0] != null && evdArr[1] != null && evdArr[2] == null && dirArr[0] != null && dirArr[1] == null){
			newCluePartList = new ArrayList<CluePart>();
			newCluePartList.add(new CluePart(evdArr[0], dirArr[0]));
			newCluePartList.add(new CluePart(evdArr[1], null));
			return true;
		}
		
		if(evdArr[0] != null && evdArr[1] != null && evdArr[2] != null && dirArr[0] != null && dirArr[1] != null){
			newCluePartList = new ArrayList<CluePart>();
			newCluePartList.add(new CluePart(evdArr[0], dirArr[0]));
			newCluePartList.add(new CluePart(evdArr[1], dirArr[1]));
			newCluePartList.add(new CluePart(evdArr[2], null));
			
			return true;
		}
		
		newCluePartList = null;
		return false;
	}
	
	public List<Clue> getClues(){
		return finalSplitClues;
	}
	
	public List<List<CluePart>> getUnsplitClues(){
		return addedCluePartList;
	}
	
	public void resetClue(){
		clearNewClueSelection();
		
		addedClueModel.removeAllElements();
		addedCluePartList.clear();
		finalSplitClues = new LinkedList<Clue>();
	}
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			UIManager.setLookAndFeel(
		            UIManager.getSystemLookAndFeelClassName());

			ClueBuilder dialog = new ClueBuilder();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
			System.out.println();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
