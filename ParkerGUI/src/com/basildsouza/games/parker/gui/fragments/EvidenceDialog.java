package com.basildsouza.games.parker.gui.fragments;

import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.AbstractButton;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.UIManager;
import javax.swing.border.TitledBorder;

import com.basildsouza.games.parker.engine.Evidence;
import com.basildsouza.games.parker.gui.util.ImageManager;

public class EvidenceDialog extends JDialog {
	private Evidence selectedEvidence;
	
	private static final long serialVersionUID = 1L;
	

	/**
	 * Create the dialog.
	 */
	public EvidenceDialog() {
		setModal(true);
		setResizable(false);
		setTitle("Evidence");
		setBounds(100, 100, 260, 285);
		getContentPane().setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		
		JPanel mainPanel = createMainPanel();
		getContentPane().add(mainPanel);
		
		JPanel statusPanel = createStatusPanel();
		getContentPane().add(statusPanel);
		
		setDefaultCloseOperation(JDialog.HIDE_ON_CLOSE);
	}
	
	private JPanel createMainPanel(){
		JPanel mainPanel = new JPanel();
		mainPanel.setLayout(new GridLayout(2, 1, 0, 0));
		
		JPanel peoplePanel = new JPanel();
		FlowLayout flowLayout = (FlowLayout) peoplePanel.getLayout();
		flowLayout.setHgap(5);
		mainPanel.add(peoplePanel);
		peoplePanel.setBorder(new TitledBorder(null, "People", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		peoplePanel.setPreferredSize(new Dimension(mainPanel.getPreferredSize().width, 105));
		buildEvidenceList(peoplePanel, true);
		
		JPanel itemPanel = new JPanel();
		FlowLayout flowLayout_1 = (FlowLayout) itemPanel.getLayout();
		flowLayout_1.setHgap(5);
		mainPanel.add(itemPanel);
		itemPanel.setBorder(new TitledBorder(null, "Items", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		itemPanel.setPreferredSize(new Dimension(mainPanel.getPreferredSize().width, 105));
		buildEvidenceList(itemPanel, false);
		
		mainPanel.setPreferredSize(new Dimension(getWidth()-7, mainPanel.getPreferredSize().height));
		return mainPanel;
	}
	
	protected JPanel createStatusPanel(){
		JPanel statusPanel = new JPanel();
		statusPanel.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		
		JLabel instructionLabel = new JLabel("Click on a Person or Item to select it.");
		statusPanel.add(instructionLabel);
		
		JButton cancelButton = new JButton("Cancel");
		cancelButton.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
			}
		});
		
		statusPanel.add(cancelButton);
		
		statusPanel.setPreferredSize(new Dimension(getWidth()-7, statusPanel.getPreferredSize().height));
		return statusPanel;
	}
	
	
	protected void buildEvidenceList(JPanel panel, boolean isPerson){
		List<Evidence> evidenceList = Evidence.getEvidence(isPerson);
		
		for(Evidence evidence : evidenceList){
			panel.add(createButton(evidence));
		}
	}
	
	protected AbstractButton createButton(final Evidence evidence){
		JButton button = new JButton("");
		button.setPreferredSize(new Dimension(32, 32));
		button.setIcon(ImageManager.get32x32Image(evidence.getSprite()));
		
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				selectedEvidence = evidence;
				setVisible(false);
			}
		});
		return button;
	}
	
	public Evidence selectEvidence(){
		selectedEvidence = null;
		
		setVisible(true);
		
		return  selectedEvidence;
	}

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					UIManager.setLookAndFeel(
				            UIManager.getSystemLookAndFeelClassName());

					EvidenceDialog dialog = new EvidenceDialog();
					//dialog.setDefaultCloseOperation(JDialog.HIDE_ON_CLOSE);
					//dialog.setVisible(true);
					System.out.println(dialog.selectEvidence());
					System.exit(0);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}


}
