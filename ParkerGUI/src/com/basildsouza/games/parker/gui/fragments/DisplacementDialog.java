package com.basildsouza.games.parker.gui.fragments;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.UIManager;
import javax.swing.border.TitledBorder;

import com.basildsouza.games.parker.engine.Direction;
import com.basildsouza.games.parker.engine.Displacement;

public class DisplacementDialog extends JDialog {
	
	private static final long serialVersionUID = 1L;
	private final JPanel contentPanel;
	
	private Displacement displacement;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			UIManager.setLookAndFeel(
		            UIManager.getSystemLookAndFeelClassName());

			DisplacementDialog dialog = new DisplacementDialog();
			
			System.out.println(dialog.selectDisplacement());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public Displacement getDisplacement() {
		return displacement;
	}
	
	public Displacement selectDisplacement(){
		setVisible(true);
		return getDisplacement();
	}

	public DisplacementDialog(Displacement displacement){
		this();
		
		setDisplacement(displacement.getDistance(), displacement.getDirection());
	}
	
	/**
	 * Create the dialog.
	 */
	public DisplacementDialog() {
		setResizable(false);
		setTitle("Displacement Dialog");
		setBounds(100, 100, 264, 183);
		setModal(true);
		getContentPane().setLayout(new BorderLayout());
		
		contentPanel = createUnifiedContentPanel();
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		
		JPanel buttonPane = createButtonPanel();
		getContentPane().add(buttonPane, BorderLayout.SOUTH);
		
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
	}
	
	public JPanel createUnifiedContentPanel(){
		JPanel contentPanel = new JPanel();
		contentPanel.setLayout(new GridLayout(0, 1, 0, 0));
		
		contentPanel.add(createDisplacementPanel());
		return contentPanel;
	}
	

	public JPanel createButtonPanel(){
		JPanel buttonPane = new JPanel();
		buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));

		JButton cancelBtn = new JButton("Cancel");
		cancelBtn.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				displacement = null;
				dispose();
			}
		});
		buttonPane.add(cancelBtn);
		
		return buttonPane;
		
	}
	
	public JPanel createDisplacementPanel(){
		JPanel disPanel = new JPanel();
		disPanel.setBorder(new TitledBorder(null, "Displacement", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		GridBagLayout gbl_disPanel = new GridBagLayout();
		gbl_disPanel.columnWidths = new int[]{32, 32, 32, 32, 32};
		gbl_disPanel.rowHeights = new int[]{32, 32, 0};
		gbl_disPanel.columnWeights = new double[]{1, 1, 1, 1, 1};
		gbl_disPanel.rowWeights = new double[]{1, 1, 1};
		disPanel.setLayout(gbl_disPanel);
		
		JButton withBtn = new JButton("+");
		withBtn.addActionListener(createDisplacementActionListener(0, Direction.EAST));
		GridBagConstraints gbc_withBtn = new GridBagConstraints();
		gbc_withBtn.fill = GridBagConstraints.BOTH;
		gbc_withBtn.insets = new Insets(0, 0, 5, 5);
		gbc_withBtn.gridx = 0;
		gbc_withBtn.gridy = 0;
		disPanel.add(withBtn, gbc_withBtn);
		
		JButton eastUnBtn = new JButton("?E");
		eastUnBtn.addActionListener(createDisplacementActionListener(-1, Direction.EAST));
		GridBagConstraints gbc_eastUnBtn = new GridBagConstraints();
		gbc_eastUnBtn.fill = GridBagConstraints.BOTH;
		gbc_eastUnBtn.insets = new Insets(0, 0, 5, 5);
		gbc_eastUnBtn.gridx = 1;
		gbc_eastUnBtn.gridy = 0;
		disPanel.add(eastUnBtn, gbc_eastUnBtn);
		
		JButton east1Btn = new JButton("1E");
		east1Btn.addActionListener(createDisplacementActionListener(1, Direction.EAST));
		GridBagConstraints gbc_east1Btn = new GridBagConstraints();
		gbc_east1Btn.fill = GridBagConstraints.BOTH;
		gbc_east1Btn.insets = new Insets(0, 0, 5, 5);
		gbc_east1Btn.gridx = 2;
		gbc_east1Btn.gridy = 0;
		disPanel.add(east1Btn, gbc_east1Btn);
		
		JButton east2Btn = new JButton("2E");
		east2Btn.addActionListener(createDisplacementActionListener(2, Direction.EAST));
		GridBagConstraints gbc_east2Btn = new GridBagConstraints();
		gbc_east2Btn.fill = GridBagConstraints.BOTH;
		gbc_east2Btn.insets = new Insets(0, 0, 5, 5);
		gbc_east2Btn.gridx = 3;
		gbc_east2Btn.gridy = 0;
		disPanel.add(east2Btn, gbc_east2Btn);
		
		JButton east3Btn = new JButton("3E");
		east3Btn.addActionListener(createDisplacementActionListener(3, Direction.EAST));
		GridBagConstraints gbc_east3Btn = new GridBagConstraints();
		gbc_east3Btn.fill = GridBagConstraints.BOTH;
		gbc_east3Btn.insets = new Insets(0, 0, 5, 0);
		gbc_east3Btn.gridx = 4;
		gbc_east3Btn.gridy = 0;
		disPanel.add(east3Btn, gbc_east3Btn);
		
		JButton southUnBtn = new JButton("?S");
		southUnBtn.addActionListener(createDisplacementActionListener(-1, Direction.SOUTH));
		GridBagConstraints gbc_southUnBtn = new GridBagConstraints();
		gbc_southUnBtn.fill = GridBagConstraints.BOTH;
		gbc_southUnBtn.insets = new Insets(0, 0, 0, 5);
		gbc_southUnBtn.gridx = 1;
		gbc_southUnBtn.gridy = 1;
		disPanel.add(southUnBtn, gbc_southUnBtn);
		
		JButton south1Btn = new JButton("1S");
		south1Btn.addActionListener(createDisplacementActionListener(1, Direction.SOUTH));
		GridBagConstraints gbc_south1Btn = new GridBagConstraints();
		gbc_south1Btn.fill = GridBagConstraints.BOTH;
		gbc_south1Btn.insets = new Insets(0, 0, 0, 5);
		gbc_south1Btn.gridx = 2;
		gbc_south1Btn.gridy = 1;
		disPanel.add(south1Btn, gbc_south1Btn);
		
		JButton south2Btn = new JButton("2S");
		south2Btn.addActionListener(createDisplacementActionListener(2, Direction.SOUTH));
		GridBagConstraints gbc_south2Btn = new GridBagConstraints();
		gbc_south2Btn.fill = GridBagConstraints.BOTH;
		gbc_south2Btn.insets = new Insets(0, 0, 0, 5);
		gbc_south2Btn.gridx = 3;
		gbc_south2Btn.gridy = 1;
		disPanel.add(south2Btn, gbc_south2Btn);
		
		JButton south3Btn = new JButton("3S");
		south3Btn.addActionListener(createDisplacementActionListener(3, Direction.SOUTH));
		GridBagConstraints gbc_south3Btn = new GridBagConstraints();
		gbc_south3Btn.fill = GridBagConstraints.BOTH;
		gbc_south3Btn.gridx = 4;
		gbc_south3Btn.gridy = 1;
		disPanel.add(south3Btn, gbc_south3Btn);
		
		
		
		return disPanel;
	}
	
	
	public ActionListener createDisplacementActionListener(final int distance, final Direction direction){
		return new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				//@note: Simpler because the work is assumed to be done on the calling part
				setDisplacement(distance, direction);
				
				dispose();				
			}
		};
	}
	
	private void setDisplacement(int distance, Direction direction){
		this.displacement = new Displacement(direction, distance);
	}


}
