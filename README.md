# Inspector Parker - Auto Solver #

This tool can help auto solve the inspector parker game

* To download: https://www.bigfishgames.com/games/94/inspectorparker/
* To play online: https://www.coolstreaming.us/forum/arcade/game/975.html

I had written this circa 2010, so many of the libraries and tools may be a bit dated.

## Game Playing instructions ##
* Read the clues provided on the elft
* If you determine where an object is then left click on it
* If you determine that an object definitely cannot be there, then hold Ctrl and then left click on it

## Using the Auto Solver ##
* Start Game
* Take screenshot and pause the game
* Start tool
* Select the size of the room
* Place the Evidence as visible in the game
* Configure the clues that you cans see in the game
* Click on Solve
* Unpause game
* Enter the solved state back to the main game

## Screenshots ##
![MainScreen](Screenshots/MainScreen.PNG "MainScreen")

![Clue Builder-Empty](Screenshots/ClueBuilder-Empty.PNG?raw=true "Clue Builder-Empty")

![ClueBuilder-Filled](/Screenshots/ClueBuilder-Filled.PNG?raw=true "ClueBuilder-Filled")

![DisplacementDialog](/Screenshots/DisplacementDialog.PNG?raw=true "DisplacementDialog")

![MultiEvidence-Empty](/Screenshots/MultiEvidence-Empty.PNG?raw=true "MultiEvidence-Empty")

![MultiEvidence-Filled](/Screenshots/MultiEvidence-Filled.PNG?raw=true "MultiEvidence-Filled")

![SingleEvidence](/Screenshots/SingleEvidence.PNG?raw=true "SingleEvidence")



# Build and Running instructions
This was written using eclipse's swing builder and doesnt have an independant build tool like maven or gradle configured for it.
So the only way to build and run it is to import it into eclipse.
