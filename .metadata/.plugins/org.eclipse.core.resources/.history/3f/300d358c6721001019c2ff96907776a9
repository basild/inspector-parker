package com.basildsouza.games.parker.gui.fragments;

import java.awt.CardLayout;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.GridLayout;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.UIManager;
import javax.swing.border.TitledBorder;

import com.basildsouza.games.parker.engine.Evidence;
import com.basildsouza.games.parker.gui.util.ImageManager;

public class RoomPanel extends JPanel {
	private JLabel[] personLblArr;
	private JLabel[] itemLblArr;
	
	private Set<Evidence> selectedPeople;
	private Set<Evidence> selectedItems;
	
	private final int xCoord;
	private final int yCoord;
	
	private final JPanel solvedPanel;
	private final JPanel unsolvedPanel;
	
	private static final String SOLVED = "SOLVED";
	private static final String UNSOLVED = "UNSOLVED";
	
	/**
	 * @wbp.parser.constructor
	 */
	public RoomPanel(int xCoord, int yCoord) {
		this(xCoord, yCoord, new HashSet<Evidence>(), new HashSet<Evidence>());
	}
	/**
	 * Create the panel.
	 */
	public RoomPanel(int xCoord, int yCoord, Set<Evidence> people, Set<Evidence> items) {
		this.xCoord = xCoord;
		this.yCoord = yCoord;
		
		this.selectedPeople = people;
		this.selectedItems = items;
		
		setBorder(new TitledBorder(null, "Room (" + xCoord + ", " + yCoord + ")", 
								   TitledBorder.LEADING, TitledBorder.TOP, null, null));
		setSize(150, 150);
		setPreferredSize(new Dimension(150, 150));
		
		JPanel lblPanel = new JPanel();
		lblPanel.setPreferredSize(new Dimension(143, 75));
		lblPanel.setLayout(new GridLayout(2, 6, 0, 0));
		
		personLblArr = createLabels(lblPanel);
		itemLblArr = createLabels(lblPanel);

		JPanel buttonPanel = new JPanel();
		
		JButton clearBtn = new JButton("Clear");
		buttonPanel.add(clearBtn);
		clearBtn.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				selectedPeople = new HashSet<Evidence>();
				selectedItems = new HashSet<Evidence>();
				
				updateLabels();
			}
		});
		
		JButton editBtn = new JButton("Edit");
		buttonPanel.add(editBtn);
		editBtn.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				MultiEvidenceDialog evidenceSelector = new MultiEvidenceDialog(new HashSet<Evidence>(selectedPeople), new HashSet<Evidence>(selectedItems));
				evidenceSelector.setVisible(true);
				
				selectedPeople = new HashSet<Evidence>(evidenceSelector.getSelectedPersonSet());
				selectedItems = new HashSet<Evidence>(evidenceSelector.getSelectedItemSet());
				
				updateLabels();
			}
		});
		
		buttonPanel.setPreferredSize(new Dimension(143, 33));
		
		unsolvedPanel = new JPanel();
		unsolvedPanel.add(lblPanel);
		unsolvedPanel.add(buttonPanel);
		unsolvedPanel.setPreferredSize(new Dimension(143, 120));
		
		solvedPanel = new JPanel();
		
		setLayout(new CardLayout());
		add(unsolvedPanel, UNSOLVED);
		add(solvedPanel, SOLVED);
		
		setSolved(false);
		updateLabels();
	}
	
	public void setSolved(boolean solved){
		CardLayout layout = (CardLayout) getLayout();
		
		layout.show(this, solved?SOLVED:UNSOLVED);
	}
	
	public JLabel[] createLabels(JPanel panel){
		JLabel[] labelArr = new JLabel[6];
		for(int i=0; i<labelArr.length; i++){
			labelArr[i] = new JLabel("");
			panel.add(labelArr[i]);
		}
		
		return labelArr;
	}
	
	
	public void setSelectedEvidence(Set<Evidence> personSet, Set<Evidence> itemSet){
		selectedPeople = new HashSet<Evidence>(personSet);
		selectedItems = new HashSet<Evidence>(itemSet);
		
		boolean solved = personSet.size() <= 1 && itemSet.size() <= 1;
		updateSelection(solved);
	}
	
	public void updateSelection(boolean solved){
		if(solved){
			updateLabels();
		} else {
			solvedPanel.removeAll();
			if(!selectedPeople.isEmpty()){
				
			}
		}
		
	}
	
	public void updateLabels(){
		updateLabels(personLblArr, selectedPeople);
		updateLabels(itemLblArr, selectedItems);
	}
	
	public void updateLabels(JLabel[] labelArr, Set<Evidence> evidenceList){
		int labelCount = 0;
		Iterator<Evidence> evIt = evidenceList.iterator();
		
		for(labelCount = 0; labelCount<labelArr.length && evIt.hasNext(); labelCount++){
			labelArr[labelCount].setIcon(ImageManager.get24x24Image(evIt.next().getSprite()));
		}
		
		for(;labelCount<labelArr.length; labelCount++){
			labelArr[labelCount].setIcon(null);
		}
	}
	
	public Set<Evidence> getSelectedItems() {
		return selectedItems;
	}
	
	public Set<Evidence> getSelectedPeople() {
		return selectedPeople;
	}
	
	public static void main(String args[]){
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					UIManager.setLookAndFeel(
				            UIManager.getSystemLookAndFeelClassName());
					
					new JDialog(){
						{
							setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
							setBounds(100, 100, 180, 180);
							setModal(true);
							Set<Evidence> personEvidence = new HashSet<Evidence>(Arrays.asList(new Evidence[]{Evidence.BORRIS, Evidence.MAURICE, Evidence.FRED}));
							Set<Evidence> itemEvidence = new HashSet<Evidence>(Arrays.asList(new Evidence[]{Evidence.KNIFE, Evidence.NOOSE, Evidence.LAUDANUM}));
							getContentPane().add(new RoomPanel(2, 3, personEvidence, itemEvidence));
						}
					}.setVisible(true);

					
					
					System.exit(0);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});

	}

}
