package com.basildsouza.games.parker.gui.fragments;

import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashSet;
import java.util.Set;

import javax.swing.AbstractButton;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JToggleButton;
import javax.swing.UIManager;
import javax.swing.border.TitledBorder;

import com.basildsouza.games.parker.engine.Evidence;
import com.basildsouza.games.parker.gui.util.ImageManager;

public class MultiEvidenceDialog extends EvidenceDialog {
	private JButton okBtn;
	
	private Set<Evidence> selectedPersonSet;
	private Set<Evidence> selectedItemSet;
	
	private Set<Evidence> originalPersonSet = new HashSet<Evidence>();
	private Set<Evidence> originalItemSet = new HashSet<Evidence>();
	

	private JLabel[] personLblArr;
	private JLabel[] itemLblArr;
	
	private static final long serialVersionUID = 1L;
	
	public Set<Evidence> getSelectedItemSet() {
		return selectedItemSet;
	}
	
	public Set<Evidence> getSelectedPersonSet() {
		return selectedPersonSet;
	}
	
	public MultiEvidenceDialog(Set<Evidence> personSet, Set<Evidence> itemSet){
		this();
		this.selectedItemSet = itemSet;
		this.selectedPersonSet = personSet;
	}
	
	/**
	 * Create the dialog.
	 */
	public MultiEvidenceDialog() {
		setBounds(100, 100, 260, 460);
		
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
	}
	
	@Override
	protected AbstractButton createButton(final Evidence evidence) {
		final JToggleButton button = new JToggleButton("");
		button.setPreferredSize(new Dimension(32, 32));
		button.setIcon(ImageManager.get32x32Image(evidence.getSprite()));
		
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(button.getModel().isSelected()){
					if(!setEvidence(evidence)){
						button.getModel().setSelected(false);
					}
				} else {
					unsetEvidence(evidence);
				}
			}
		});
		
		return button;
	}
	
	private boolean setEvidence(Evidence evidence){
		Set<Evidence> selectedSet = evidence.isPerson()?selectedPersonSet:selectedItemSet;
		
		if(selectedSet.size() >= personLblArr.length){
			JOptionPane.showMessageDialog(this, "You may not select more than 6 of a kind", 
										  "Invalid Selection", JOptionPane.ERROR_MESSAGE);
			return false;
		}
		selectedSet.add(evidence);
		
		validateEntries();
		return true;
	}
	
	private boolean unsetEvidence(Evidence evidence){
		Set<Evidence> selectedSet = evidence.isPerson()?selectedPersonSet:selectedItemSet;
		
		selectedSet.remove(evidence);
		
		validateEntries();
		return true;
	}
	
	private void validateEntries(){
		updateLabels();
		boolean isValid = !(selectedItemSet.isEmpty() && selectedPersonSet.isEmpty()); 
		okBtn.setEnabled(isValid);
	}
	
	private void updateLabels(){
		updateLabels(selectedItemSet, itemLblArr);
		updateLabels(selectedPersonSet, personLblArr);
	}
	
	private void updateLabels(Set<Evidence> evidenceSet, JLabel[] labelArr){
		int placeHolder = 0;
		for(Evidence evd : evidenceSet){
			if(placeHolder >= labelArr.length){
				//TODO: Log the error here
				break;
			}
			labelArr[placeHolder].setIcon(ImageManager.get32x32Image(evd.getSprite()));
			placeHolder ++;
		}
		for(int i=placeHolder; i<labelArr.length; i++){
			labelArr[i].setIcon(null);
		}
	}

	@Override
	protected JPanel createStatusPanel() {
		JPanel statusPanel = new JPanel();
		statusPanel.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		statusPanel.setPreferredSize(new Dimension(getWidth()-7, 205));
		
		JPanel selectedPanel = createSelectedPanel();
		
		selectedPanel.setPreferredSize(
					new Dimension(statusPanel.getPreferredSize().width, 
								  165));
		statusPanel.add(selectedPanel);
		
		JPanel buttonPanel = new JPanel();
		statusPanel.add(buttonPanel);
		
		okBtn = new JButton("OK");
		buttonPanel.add(okBtn);
		
		JButton cancelBtn = new JButton("Cancel");
		buttonPanel.add(cancelBtn);
		cancelBtn.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
			}
		});
		
		okBtn.setPreferredSize(cancelBtn.getPreferredSize());
		buttonPanel.setPreferredSize(new Dimension(statusPanel.getPreferredSize().width, buttonPanel.getPreferredSize().height));

		return statusPanel;
	}
	
	protected JPanel createSelectedPanel(){
		JPanel selectedPanel = new JPanel();
		selectedPanel.setBorder(new TitledBorder(null, "Selection", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		
		GridBagLayout gbl_selectedPanel = new GridBagLayout();
		gbl_selectedPanel.columnWidths = new int[]{32, 32, 32, 32, 32, 32};
		gbl_selectedPanel.rowHeights = new int[]{0, 0, 37, 0, 37, 0};
		gbl_selectedPanel.columnWeights = new double[]{1.0, 1.0, 1.0, 1.0, 1.0, 1.0};
		gbl_selectedPanel.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0};
		selectedPanel.setLayout(gbl_selectedPanel);
		
		JLabel instructionLbl = new JLabel("Click above to select/unselect evidence.");
		instructionLbl.setPreferredSize(new Dimension(selectedPanel.getPreferredSize().width, instructionLbl.getPreferredSize().height));
		GridBagConstraints gbc_instructionLbl = new GridBagConstraints();
		gbc_instructionLbl.anchor = GridBagConstraints.NORTH;
		gbc_instructionLbl.insets = new Insets(0, 0, 5, 0);
		gbc_instructionLbl.gridwidth = 6;
		gbc_instructionLbl.gridx = 0;
		gbc_instructionLbl.gridy = 0;
		selectedPanel.add(instructionLbl, gbc_instructionLbl);
		
		JLabel personLbl = new JLabel("Selected People:");
		GridBagConstraints gbc_personLbl = new GridBagConstraints();
		gbc_personLbl.anchor = GridBagConstraints.WEST;
		gbc_personLbl.gridwidth = 3;
		gbc_personLbl.insets = new Insets(0, 0, 5, 5);
		gbc_personLbl.gridx = 0;
		gbc_personLbl.gridy = 1;
		selectedPanel.add(personLbl, gbc_personLbl);

		personLblArr = new JLabel[6];
		itemLblArr = new JLabel[6];

		JLabel perIco0Lbl = new JLabel("");
		GridBagConstraints gbc_perIco0Lbl = new GridBagConstraints();
		gbc_perIco0Lbl.insets = new Insets(0, 0, 5, 5);
		gbc_perIco0Lbl.gridx = 0;
		gbc_perIco0Lbl.gridy = 2;
		selectedPanel.add(perIco0Lbl, gbc_perIco0Lbl);
		personLblArr[0] = perIco0Lbl;
		
		JLabel perIco1Lbl = new JLabel("");
		GridBagConstraints gbc_perIco1Lbl = new GridBagConstraints();
		gbc_perIco1Lbl.insets = new Insets(0, 0, 5, 5);
		gbc_perIco1Lbl.gridx = 1;
		gbc_perIco1Lbl.gridy = 2;
		selectedPanel.add(perIco1Lbl, gbc_perIco1Lbl);
		personLblArr[1] = perIco1Lbl;
		
		JLabel perIco2Lbl = new JLabel("");
		GridBagConstraints gbc_perIco2Lbl = new GridBagConstraints();
		gbc_perIco2Lbl.insets = new Insets(0, 0, 5, 5);
		gbc_perIco2Lbl.gridx = 2;
		gbc_perIco2Lbl.gridy = 2;
		selectedPanel.add(perIco2Lbl, gbc_perIco2Lbl);
		personLblArr[2] = perIco2Lbl;
		
		JLabel perIco3Lbl = new JLabel("");
		GridBagConstraints gbc_perIco3Lbl = new GridBagConstraints();
		gbc_perIco3Lbl.insets = new Insets(0, 0, 5, 5);
		gbc_perIco3Lbl.gridx = 3;
		gbc_perIco3Lbl.gridy = 2;
		selectedPanel.add(perIco3Lbl, gbc_perIco3Lbl);
		personLblArr[3] = perIco3Lbl;
		
		JLabel perIco4Lbl = new JLabel("");
		GridBagConstraints gbc_perIco4Lbl = new GridBagConstraints();
		gbc_perIco4Lbl.insets = new Insets(0, 0, 5, 5);
		gbc_perIco4Lbl.gridx = 4;
		gbc_perIco4Lbl.gridy = 2;
		selectedPanel.add(perIco4Lbl, gbc_perIco4Lbl);
		personLblArr[4] = perIco4Lbl;
		
		JLabel perIco5Lbl = new JLabel("");
		GridBagConstraints gbc_perIco5Lbl = new GridBagConstraints();
		gbc_perIco5Lbl.insets = new Insets(0, 0, 5, 0);
		gbc_perIco5Lbl.gridx = 5;
		gbc_perIco5Lbl.gridy = 2;
		selectedPanel.add(perIco5Lbl, gbc_perIco5Lbl);
		personLblArr[5] = perIco5Lbl;
		
		JLabel itemLbl = new JLabel("Select Items:");
		GridBagConstraints gbc_itemLbl = new GridBagConstraints();
		gbc_itemLbl.anchor = GridBagConstraints.WEST;
		gbc_itemLbl.gridwidth = 3;
		gbc_itemLbl.insets = new Insets(0, 0, 5, 5);
		gbc_itemLbl.gridx = 0;
		gbc_itemLbl.gridy = 3;
		selectedPanel.add(itemLbl, gbc_itemLbl);
		
		JLabel itmIco0Lbl = new JLabel("");
		GridBagConstraints gbc_itmIco0Lbl = new GridBagConstraints();
		gbc_itmIco0Lbl.insets = new Insets(0, 0, 5, 5);
		gbc_itmIco0Lbl.gridx = 0;
		gbc_itmIco0Lbl.gridy = 4;
		selectedPanel.add(itmIco0Lbl, gbc_itmIco0Lbl);
		itemLblArr[0] = itmIco0Lbl;
		
		JLabel itmIco1Lbl = new JLabel("");
		GridBagConstraints gbc_itmIco1Lbl = new GridBagConstraints();
		gbc_itmIco1Lbl.insets = new Insets(0, 0, 5, 5);
		gbc_itmIco1Lbl.gridx = 1;
		gbc_itmIco1Lbl.gridy = 4;
		selectedPanel.add(itmIco1Lbl, gbc_itmIco1Lbl);
		itemLblArr[1] = itmIco1Lbl;
		
		JLabel itmIco2Lbl = new JLabel("");
		GridBagConstraints gbc_itmIco2Lbl = new GridBagConstraints();
		gbc_itmIco2Lbl.insets = new Insets(0, 0, 5, 5);
		gbc_itmIco2Lbl.gridx = 2;
		gbc_itmIco2Lbl.gridy = 4;
		selectedPanel.add(itmIco2Lbl, gbc_itmIco2Lbl);
		itemLblArr[2] = itmIco2Lbl;
		
		JLabel itmIco3Lbl = new JLabel("");
		GridBagConstraints gbc_itmIco3Lbl = new GridBagConstraints();
		gbc_itmIco3Lbl.insets = new Insets(0, 0, 5, 5);
		gbc_itmIco3Lbl.gridx = 3;
		gbc_itmIco3Lbl.gridy = 4;
		selectedPanel.add(itmIco3Lbl, gbc_itmIco3Lbl);
		itemLblArr[3] = itmIco3Lbl;
		
		JLabel itmIco4Lbl = new JLabel("");
		GridBagConstraints gbc_itmIco4Lbl = new GridBagConstraints();
		gbc_itmIco4Lbl.insets = new Insets(0, 0, 5, 5);
		gbc_itmIco4Lbl.gridx = 4;
		gbc_itmIco4Lbl.gridy = 4;
		selectedPanel.add(itmIco4Lbl, gbc_itmIco4Lbl);
		itemLblArr[4] = itmIco4Lbl;
		
		JLabel itmIco5Lbl = new JLabel("");
		GridBagConstraints gbc_itmIco5Lbl = new GridBagConstraints();
		gbc_itmIco5Lbl.insets = new Insets(0, 0, 5, 0);
		gbc_itmIco5Lbl.gridx = 5;
		gbc_itmIco5Lbl.gridy = 4;
		selectedPanel.add(itmIco5Lbl, gbc_itmIco5Lbl);
		itemLblArr[5] = itmIco5Lbl;
		
		return selectedPanel;
	}
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					UIManager.setLookAndFeel(
				            UIManager.getSystemLookAndFeelClassName());
					
					MultiEvidenceDialog dialog = new MultiEvidenceDialog();
					dialog.setVisible(true);
					System.out.println(dialog.getSelectedItemSet());
					System.out.println(dialog.getSelectedPersonSet());
					
					System.exit(0);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
}
