package com.basildsouza.games.parker.engine;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class RoomManager {
	private static Room[][] rooms;
	private static List<Clue> clues;
	private static boolean changed;
	private static Map<Evidence, List<Room>> evidenceCounter = new HashMap<Evidence, List<Room>>();
	
	private static Map<Evidence, Room> selectedMap = new HashMap<Evidence, Room>();
	
	private static StringBuilder solutionLogger = new StringBuilder(10000);
	
	public static void updateEvidenceCount(Room room, Set<Evidence> evidenceSet){
		for(Evidence evidence : evidenceSet){
			List<Room> countList = evidenceCounter.get(evidence);
			
			if(countList == null){
				countList = new LinkedList<Room>();
				evidenceCounter.put(evidence, countList);
			}
			countList.add(room);			
		}		
	}

	public static boolean removeEvidence(Room room, Evidence evidence){
		List<Room> roomList = evidenceCounter.get(evidence);
		if(roomList == null){
			if(selectedMap.containsKey(evidence)){
				System.err.println("Warning: Trying to remove a selected object!");
				return false;
			}
		}
		notifyChange();
		boolean found = roomList.remove(room);
		// If just 1 person found, then might as well process it now only:
		if(roomList.size() == 1){
			Room foundRoom = roomList.get(0);
			System.out.println("Room (" + room.getXPos() + ", " + 
					   room.getYPos() + ") Selecting: " + 
					   evidence + " While Simplifying. (By Isolation)" );
			foundRoom.selectEvidence(evidence);
			
			selectedMap.put(evidence, foundRoom);
			evidenceCounter.remove(evidence);
		}
		
		return found;
	}
	
	public static Room[][] solveCrime(Room[][] rooms, List<Clue> clues){
		updateNeighbours(rooms);
		RoomManager.rooms = rooms;
		RoomManager.clues = clues;
		buildEvidenceCount();
		int counter = 0;
		do {
			changed = false;
			System.out.println("--- Iteration " + counter + " ---");
			for(Clue clue : clues){
				for(int i=0; i<rooms.length; i++){
					for(int j=0; j<rooms[i].length; j++){
						clue.evaluate(rooms[i][j]);
					}
				}
				//System.out.println("--Simplifying " + counter + " --");
				simplify();
				//System.out.println("--End Simplifying " + counter + " --");
			}
			counter ++;
		} while (changed);
		System.out.println("Selected Evidence: ");
		System.out.println(selectedMap);
		
		System.out.println("Unaccounted for Evidence:");
		System.out.println(evidenceCounter);
		return rooms;
	}
	
	private static void updateNeighbours(Room[][] rooms){
		for(int i=0; i<rooms.length; i++){
			for(int j=0; j<rooms[i].length; j++){
				Room[] neighbours = new Room[4];
				//TODO: Problem in the x and y axis thingie
				if(j-1 >= 0){
					neighbours[Direction.NORTH.ordinal()] = rooms[i][j-1];
				}
				if(i+1 < rooms.length){
					neighbours[Direction.EAST.ordinal()] = rooms[i+1][j];
				}
				if(j+1 < rooms[i].length){
					neighbours[Direction.SOUTH.ordinal()] = rooms[i][j+1];
				}
				if(i-1 >= 0){
					neighbours[Direction.WEST.ordinal()] = rooms[i-1][j];
				}
				rooms[i][j].setNeighbours(neighbours);
			}
		}

	}
	
	public static void printCurrRooms(){
		printRooms(rooms);
	}
	
	private static void buildEvidenceCount(){
		for(int i=0; i<rooms.length; i++){
			for(int j=0; j<rooms[i].length; j++){
				Room room = rooms[i][j];
				updateEvidenceCount(room, room.getPersonSet());
				updateEvidenceCount(room, room.getItemSet());
			}
		}
	}
	public static void printRooms(Room[][] rooms){
		for(int i=0; i<rooms[0].length; i++){
			String[] lineBuf = new String[]{"", "", "", ""};
			for(int j=0; j<rooms.length; j++){
				String[] parts = rooms[j][i].getMembers().split("\\n");
				for(int l=0; l<4; l++){
					lineBuf[l] += String.format("%-30s", parts[l]); 
				}
			}
			for(int l=0; l<4; l++){
				System.out.println(lineBuf[l]);
			}
			System.out.println();
		}
	}

	public static void simplify(){
		// Items to simplify:
		// If an item/person exists in only one
		//while(solveSingleOccurances(rooms));
		//while(solveSingleOccurances_old(rooms));
	}
	private static boolean solveSingleOccurances(Room[][] rooms){
		boolean changed = false;
		Set<Entry<Evidence, List<Room>>> entrySet = evidenceCounter.entrySet();
		Iterator<Entry<Evidence, List<Room>>> iterator = entrySet.iterator();
		while(iterator.hasNext()){
			Entry<Evidence, List<Room>> entry = iterator.next();
			if(entry.getValue().size() == 1){
				Room room = entry.getValue().get(0);
				Evidence evidence = entry.getKey();
				System.out.println("Room (" + room.getXPos() + ", " + 
						   room.getYPos() + ") Selecting: " + 
						   evidence + " While Simplifying. (By Isolation)" );
				room.selectEvidence(evidence);
				changed = true;
				iterator.remove();
				selectedMap.put(evidence, room);
			}
		}
		
		return changed;
	}

	public static void handleSelection(Room firingRoom, Evidence evidence){
		for(int i=0; i<rooms.length; i++){
			for(int j=0; j<rooms[i].length; j++){
				if(rooms[i][j] == firingRoom){
					continue;
				}
				
				if(rooms[i][j].wouldRemove(evidence)){
					System.out.println("Room (" + rooms[i][j].getXPos() + ", " + 
									   rooms[i][j].getYPos() + ") Eliminating: " + 
									   evidence + " While Simplifying (By Selection)." );
				}
				rooms[i][j].removeEvidence(evidence);
			}
		}
	}
	
	public static void notifyChange(){
		changed = true;
	}
	
	public static void logStep(String step){
		
	}
}
