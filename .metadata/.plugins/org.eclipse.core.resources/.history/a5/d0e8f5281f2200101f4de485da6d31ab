package com.basildsouza.games.parker.gui.fragments;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ButtonGroup;
import javax.swing.ButtonModel;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JToggleButton;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;

import com.basildsouza.games.parker.engine.Direction;
import com.basildsouza.games.parker.engine.Displacement;

public class DisplacementDialog extends JDialog {
	
	private static final long serialVersionUID = 1L;
	private final JPanel contentPanel;
	private final ButtonGroup distanceBtnGrp = new ButtonGroup();
	private final ButtonGroup directionBtnGrp = new ButtonGroup();
	private JLabel dirSelLbl;
	private JLabel disSelLbl;
	
	private int distance;
	private Direction direction;
	
	private Displacement displacement;
	private JButton okBtn;
	private JPanel disBtnPanel;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			UIManager.setLookAndFeel(
		            UIManager.getSystemLookAndFeelClassName());

			DisplacementDialog dialog = new DisplacementDialog();
			
			System.out.println(dialog.selectDisplacement());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public Displacement getDisplacement() {
		return displacement;
	}
	
	public Displacement selectDisplacement(){
		setVisible(true);
		return getDisplacement();
	}

	public DisplacementDialog(Displacement displacement){
		this();
		
		setDisplacement(displacement.getDirection());
		setDisplacement(displacement.getDistance());
		
		validateEntries();
	}
	
	/**
	 * Create the dialog.
	 */
	public DisplacementDialog() {
		setResizable(false);
		setTitle("Displacement Dialog");
		setBounds(100, 100, 264, 183);
		setModal(true);
		getContentPane().setLayout(new BorderLayout());
		
		contentPanel = createUnifiedContentPanel();
		//contentPanel = createTwoContentPanel();
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		
		JPanel buttonPane = createButtonPanel();
		getContentPane().add(buttonPane, BorderLayout.SOUTH);
		
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);

		validateEntries();
		
	}
	
	public JPanel createUnifiedContentPanel(){
		JPanel contentPanel = new JPanel();
		contentPanel.setLayout(new GridLayout(0, 1, 0, 0));
		
		contentPanel.add(createDisplacementPanel());
		return contentPanel;
	}
	
	public JPanel createTwoContentPanel(){
		JPanel contentPanel = new JPanel();
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		
		GridBagLayout gbl_contentPanel = new GridBagLayout();
		gbl_contentPanel.columnWidths = new int[]{0, 0};
		gbl_contentPanel.rowHeights = new int[]{0, 0, 0};
		gbl_contentPanel.columnWeights = new double[]{1.0, Double.MIN_VALUE};
		gbl_contentPanel.rowWeights = new double[]{1.0, 1.0, Double.MIN_VALUE};
		contentPanel.setLayout(gbl_contentPanel);

		GridBagConstraints gbc_dirPanel = new GridBagConstraints();
		gbc_dirPanel.insets = new Insets(0, 0, 5, 0);
		gbc_dirPanel.fill = GridBagConstraints.BOTH;
		gbc_dirPanel.gridx = 0;
		gbc_dirPanel.gridy = 0;

		JPanel dirPanel = createDirectionPanel();
		contentPanel.add(dirPanel, gbc_dirPanel);
		
		JPanel disPanel = createDistancePanel();
		GridBagConstraints gbc_disPanel = new GridBagConstraints();
		gbc_disPanel.fill = GridBagConstraints.BOTH;
		gbc_disPanel.gridx = 0;
		gbc_disPanel.gridy = 1;
		contentPanel.add(disPanel, gbc_disPanel);
		
		return contentPanel;

	}

	public JPanel createButtonPanel(){
		JPanel buttonPane = new JPanel();
		buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));

		JButton clearBtn = new JButton("Clear");
		clearBtn.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				setDisplacement(null);
				setDisplacement(-2);
				setEnabledDistanceButtons(true);
				unselectAllButtons(directionBtnGrp);
				unselectAllButtons(distanceBtnGrp);
				validateEntries();
			}
		});
		buttonPane.add(clearBtn);
		
		okBtn = new JButton("OK");
		okBtn.setActionCommand("OK");
		okBtn.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		//buttonPane.add(okBtn);
		//getRootPane().setDefaultButton(okBtn);
		getRootPane().setDefaultButton(clearBtn);

		//okBtn.setPreferredSize(clearBtn.getPreferredSize());
		
		return buttonPane;
		
	}
	
	public JPanel createDisplacementPanel(){
		JPanel disPanel = new JPanel();
		disPanel.setBorder(new TitledBorder(null, "Displacement", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		GridBagLayout gbl_disPanel = new GridBagLayout();
		gbl_disPanel.columnWidths = new int[]{32, 32, 32, 32, 32};
		gbl_disPanel.rowHeights = new int[]{32, 32, 0};
		gbl_disPanel.columnWeights = new double[]{1, 1, 1, 1, 1};
		gbl_disPanel.rowWeights = new double[]{1, 1, 1};
		disPanel.setLayout(gbl_disPanel);
		
		JButton withBtn = new JButton("+");
		withBtn.addActionListener(createDisplacementActionListener(0, Direction.EAST));
		GridBagConstraints gbc_withBtn = new GridBagConstraints();
		gbc_withBtn.fill = GridBagConstraints.BOTH;
		gbc_withBtn.insets = new Insets(0, 0, 5, 5);
		gbc_withBtn.gridx = 0;
		gbc_withBtn.gridy = 0;
		disPanel.add(withBtn, gbc_withBtn);
		
		JButton eastUnBtn = new JButton("?E");
		eastUnBtn.addActionListener(createDisplacementActionListener(-1, Direction.EAST));
		GridBagConstraints gbc_eastUnBtn = new GridBagConstraints();
		gbc_eastUnBtn.fill = GridBagConstraints.BOTH;
		gbc_eastUnBtn.insets = new Insets(0, 0, 5, 5);
		gbc_eastUnBtn.gridx = 1;
		gbc_eastUnBtn.gridy = 0;
		disPanel.add(eastUnBtn, gbc_eastUnBtn);
		
		JButton east1Btn = new JButton("1E");
		east1Btn.addActionListener(createDisplacementActionListener(1, Direction.EAST));
		GridBagConstraints gbc_east1Btn = new GridBagConstraints();
		gbc_east1Btn.fill = GridBagConstraints.BOTH;
		gbc_east1Btn.insets = new Insets(0, 0, 5, 5);
		gbc_east1Btn.gridx = 2;
		gbc_east1Btn.gridy = 0;
		disPanel.add(east1Btn, gbc_east1Btn);
		
		JButton east2Btn = new JButton("2E");
		east2Btn.addActionListener(createDisplacementActionListener(2, Direction.EAST));
		GridBagConstraints gbc_east2Btn = new GridBagConstraints();
		gbc_east2Btn.fill = GridBagConstraints.BOTH;
		gbc_east2Btn.insets = new Insets(0, 0, 5, 5);
		gbc_east2Btn.gridx = 3;
		gbc_east2Btn.gridy = 0;
		disPanel.add(east2Btn, gbc_east2Btn);
		
		JButton east3Btn = new JButton("3E");
		east3Btn.addActionListener(createDisplacementActionListener(3, Direction.EAST));
		GridBagConstraints gbc_east3Btn = new GridBagConstraints();
		gbc_east3Btn.fill = GridBagConstraints.BOTH;
		gbc_east3Btn.insets = new Insets(0, 0, 5, 0);
		gbc_east3Btn.gridx = 4;
		gbc_east3Btn.gridy = 0;
		disPanel.add(east3Btn, gbc_east3Btn);
		
		JButton southUnBtn = new JButton("?S");
		southUnBtn.addActionListener(createDisplacementActionListener(-1, Direction.SOUTH));
		GridBagConstraints gbc_southUnBtn = new GridBagConstraints();
		gbc_southUnBtn.fill = GridBagConstraints.BOTH;
		gbc_southUnBtn.insets = new Insets(0, 0, 0, 5);
		gbc_southUnBtn.gridx = 1;
		gbc_southUnBtn.gridy = 1;
		disPanel.add(southUnBtn, gbc_southUnBtn);
		
		JButton south1Btn = new JButton("1S");
		south1Btn.addActionListener(createDisplacementActionListener(1, Direction.SOUTH));
		GridBagConstraints gbc_south1Btn = new GridBagConstraints();
		gbc_south1Btn.fill = GridBagConstraints.BOTH;
		gbc_south1Btn.insets = new Insets(0, 0, 0, 5);
		gbc_south1Btn.gridx = 2;
		gbc_south1Btn.gridy = 1;
		disPanel.add(south1Btn, gbc_south1Btn);
		
		JButton south2Btn = new JButton("2S");
		south2Btn.addActionListener(createDisplacementActionListener(2, Direction.SOUTH));
		GridBagConstraints gbc_south2Btn = new GridBagConstraints();
		gbc_south2Btn.fill = GridBagConstraints.BOTH;
		gbc_south2Btn.insets = new Insets(0, 0, 0, 5);
		gbc_south2Btn.gridx = 3;
		gbc_south2Btn.gridy = 1;
		disPanel.add(south2Btn, gbc_south2Btn);
		
		JButton south3Btn = new JButton("3S");
		south3Btn.addActionListener(createDisplacementActionListener(3, Direction.SOUTH));
		GridBagConstraints gbc_south3Btn = new GridBagConstraints();
		gbc_south3Btn.fill = GridBagConstraints.BOTH;
		gbc_south3Btn.gridx = 4;
		gbc_south3Btn.gridy = 1;
		disPanel.add(south3Btn, gbc_south3Btn);
		
		
		
		return disPanel;
	}
	
	public JPanel createDirectionPanel(){
		JPanel dirPanel = new JPanel();
		dirPanel.setBorder(new TitledBorder(null, "Direction", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		
		dirPanel.setLayout(new GridLayout(0, 1, 0, 0));
		
		JPanel dirBtnPanel = new JPanel();
		dirPanel.add(dirBtnPanel);
		
		JToggleButton dirWithBtn = new JToggleButton("+");
		dirWithBtn.addActionListener(createDirectionActionListener(null));
		directionBtnGrp.add(dirWithBtn);
		dirBtnPanel.add(dirWithBtn);
		
		JToggleButton dirEastBtn = new JToggleButton("E");
		dirEastBtn.addActionListener(createDirectionActionListener(Direction.EAST));
		directionBtnGrp.add(dirEastBtn);
		dirBtnPanel.add(dirEastBtn);
		
		JToggleButton dirSouthBtn = new JToggleButton("S");
		dirSouthBtn.addActionListener(createDirectionActionListener(Direction.SOUTH));
		directionBtnGrp.add(dirSouthBtn);
		dirBtnPanel.add(dirSouthBtn);
		
		dirSelLbl = new JLabel("");
		dirPanel.add(dirSelLbl);
		
		return dirPanel;
	}

	public JPanel createDistancePanel(){
		JPanel disPanel = new JPanel();
		disPanel.setBorder(new TitledBorder(null, "Distance", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		disPanel.setLayout(new GridLayout(0, 1, 0, 0));
		
		disBtnPanel = new JPanel();
		disPanel.add(disBtnPanel);
		
		JToggleButton disUnknownBtn = new JToggleButton("?");
		disUnknownBtn.addActionListener(createDistanceActionListener(-1));
		distanceBtnGrp.add(disUnknownBtn);
		disBtnPanel.add(disUnknownBtn);
		
		JToggleButton dis1Btn = new JToggleButton("1");
		dis1Btn.addActionListener(createDistanceActionListener(1));
		distanceBtnGrp.add(dis1Btn);
		disBtnPanel.add(dis1Btn);
		
		JToggleButton dis2Btn = new JToggleButton("2");
		dis2Btn.addActionListener(createDistanceActionListener(2));
		distanceBtnGrp.add(dis2Btn);
		disBtnPanel.add(dis2Btn);
		
		JToggleButton dis3Btn = new JToggleButton("3");
		dis3Btn.addActionListener(createDistanceActionListener(3));
		distanceBtnGrp.add(dis3Btn);
		disBtnPanel.add(dis3Btn);
		
		JToggleButton dis4Btn = new JToggleButton("4");
		dis4Btn.addActionListener(createDistanceActionListener(4));
		distanceBtnGrp.add(dis4Btn);
		disBtnPanel.add(dis4Btn);
		
		disSelLbl = new JLabel("");
		disPanel.add(disSelLbl);
		
		return disPanel;
	}
	
	public ActionListener createDisplacementActionListener(final int distance, final Direction direction){
		return new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				//@note: Simpler because the work is assumed to be done on the calling part
				setDisplacement(direction);
				setDisplacement(distance);
				
				dispose();				
			}
		};
	}
	
	public ActionListener createDistanceActionListener(final int distance){
		return new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setDisplacement(distance);
			}
		};
	}
	
	private void setEnabledDistanceButtons(boolean enabled){
		if(!enabled){
			unselectAllButtons(distanceBtnGrp);
		}
		disBtnPanel.setEnabled(enabled);
		
		for(Component component : disBtnPanel.getComponents()){
			component.setEnabled(enabled);
		}
	}
	
	private void unselectAllButtons(ButtonGroup btnGrp){
		btnGrp.clearSelection();
	}
	
	public ActionListener createDirectionActionListener(final Direction direction){
		return new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Direction tempDir = direction;
				
				if(tempDir == null){
					tempDir = Direction.EAST;
					setDisplacement(0);
					setEnabledDistanceButtons(false);
				} else { 
					if(distance == 0){
						setDisplacement(-2);
					}
					setEnabledDistanceButtons(true);
				}
				setDisplacement(tempDir);
			}
		};
	}
	
	private void setDisplacement(int distance){
		this.distance = distance;
		
		validateEntries();
	}
	
	private void setDisplacement(Direction direction){
		this.direction = direction;
		
		validateEntries();
	}
	
	private void validateEntries(){
		displacement = new Displacement(direction, distance);
	}
	
	private boolean isDisplacementValid(){
		if(distance < -1){
			return false;
		}
		if(direction == null){
			return false;
		}
		
		return true;
	}

}
