package com.basildsouza.games.parker.gui.fragments;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import java.util.Set;

import javax.swing.ButtonGroup;
import javax.swing.ButtonModel;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JToggleButton;
import javax.swing.ListSelectionModel;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;

import com.basildsouza.games.parker.engine.Clue;
import com.basildsouza.games.parker.engine.CluePart;
import com.basildsouza.games.parker.engine.Evidence;
import com.basildsouza.games.parker.engine.Room;
import com.basildsouza.games.parker.engine.RoomArranger;
import com.basildsouza.games.parker.engine.RoomManager;
import com.basildsouza.games.parker.engine.RoomArranger.RoomLayout;

public class RoomBuilder extends JFrame {
	private final DefaultListModel clueListModel = new DefaultListModel();
	private List<List<CluePart>> unsplitClueList;
	
	private JPanel contentPane;
	private final ButtonGroup xBtnGrp = new ButtonGroup();
	private final ButtonGroup yBtnGrp = new ButtonGroup();
	
	private final ClueBuilder clueBuilderDialog = new ClueBuilder();
	
	private final DimensionListener dimensionListener = new DimensionListener();
	private final Dimension boardDimension = new Dimension();
	
	private static final long serialVersionUID = 1L;
	private JPanel roomWrapper;
	private RoomPanel[][] roomPanels;
	private JList list;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					UIManager.setLookAndFeel(
				            UIManager.getSystemLookAndFeelClassName());
					RoomBuilder frame = new RoomBuilder();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public RoomBuilder() {
		setTitle("Sherlock");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 50, 810, 680);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		roomWrapper = new JPanel();
		roomWrapper.setBorder(new TitledBorder(null, "Rooms", 
							TitledBorder.LEADING, TitledBorder.TOP, null, null));
		
		contentPane.add(roomWrapper, BorderLayout.CENTER);
		
		JPanel controlPanel = new JPanel();
		controlPanel.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), 
								"Control Panel", TitledBorder.LEADING, TitledBorder.TOP, 
								null, new Color(0, 70, 213)));
		contentPane.add(controlPanel, BorderLayout.EAST);
		
		JPanel boundsPanel = new JPanel();
		controlPanel.add(boundsPanel);
		GridBagLayout gbl_boundsPanel = new GridBagLayout();
		gbl_boundsPanel.columnWidths = new int[]{0};
		gbl_boundsPanel.rowHeights = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
		gbl_boundsPanel.columnWeights = new double[]{1.0};
		gbl_boundsPanel.rowWeights = new double[]{1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1};
		boundsPanel.setLayout(gbl_boundsPanel);
		
		JPanel xPanel = createWidthPanel();
		GridBagConstraints gbc_xPanel = new GridBagConstraints();
		gbc_xPanel.gridheight = 2;
		gbc_xPanel.fill = GridBagConstraints.BOTH;
		gbc_xPanel.insets = new Insets(0, 0, 5, 0);
		gbc_xPanel.gridx = 0;
		gbc_xPanel.gridy = 0;
		boundsPanel.add(xPanel, gbc_xPanel);

		JPanel yPanel = createHeightPanel();
		
		GridBagConstraints gbc_yPanel = new GridBagConstraints();
		gbc_yPanel.gridheight = 2;
		gbc_yPanel.fill = GridBagConstraints.BOTH;
		gbc_yPanel.insets = new Insets(0, 0, 5, 0);
		gbc_yPanel.gridx = 0;
		gbc_yPanel.gridy = 2;
		boundsPanel.add(yPanel, gbc_yPanel);
		
		JLabel clueLbl = new JLabel("Clues:");
		GridBagConstraints gbc_lblClues = new GridBagConstraints();
		gbc_lblClues.anchor = GridBagConstraints.SOUTHWEST;
		gbc_lblClues.insets = new Insets(0, 0, 5, 0);
		gbc_lblClues.gridx = 0;
		gbc_lblClues.gridy = 6;
		boundsPanel.add(clueLbl, gbc_lblClues);
		
		JScrollPane scrollPane = new JScrollPane();
		GridBagConstraints gbc_scrollPane = new GridBagConstraints();
		gbc_scrollPane.gridheight = 8;
		gbc_scrollPane.fill = GridBagConstraints.BOTH;
		gbc_scrollPane.insets = new Insets(0, 0, 5, 0);
		gbc_scrollPane.gridx = 0;
		gbc_scrollPane.gridy = 7;
		boundsPanel.add(scrollPane, gbc_scrollPane);
		
		list = new JList();
		scrollPane.setViewportView(list);
		list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		clueListModel.addElement("");
		list.setModel(clueListModel);
		
		list.setVisibleRowCount(8);
		
		JButton clueBtn = new JButton("Edit Clues");
		clueBtn.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				clueBuilderDialog.setVisible(true);
				unsplitClueList = clueBuilderDialog.getUnsplitClues();
				clueListModel.removeAllElements();
				for(List<CluePart> unsplitClue : unsplitClueList){
					clueListModel.addElement(CluePart.toString(unsplitClue));
				}
			}
		});
		
		GridBagConstraints gbc_btnAddClues = new GridBagConstraints();
		gbc_btnAddClues.fill = GridBagConstraints.BOTH;
		gbc_btnAddClues.insets = new Insets(0, 0, 5, 0);
		gbc_btnAddClues.gridx = 0;
		gbc_btnAddClues.gridy = 15;
		boundsPanel.add(clueBtn, gbc_btnAddClues);
		
		JButton solveBtn = new JButton("Solve");
		//TODO: This needs to be a lot more grand!
		solveBtn.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				displaySolvedBoard(solveCrime());
				
				//TODO: 
				// Find a way to pass a string buffer to get all the solving notes
				
			}
		});
		GridBagConstraints gbc_btnSolve = new GridBagConstraints();
		gbc_btnSolve.fill = GridBagConstraints.BOTH;
		gbc_btnSolve.insets = new Insets(0, 0, 5, 0);
		gbc_btnSolve.gridx = 0;
		gbc_btnSolve.gridy = 16;
		boundsPanel.add(solveBtn, gbc_btnSolve);
		
		JButton showLogBtn = new JButton("Show Log");
		GridBagConstraints gbc_btnShowLog = new GridBagConstraints();
		gbc_btnShowLog.insets = new Insets(0, 0, 5, 0);
		gbc_btnShowLog.fill = GridBagConstraints.BOTH;
		gbc_btnShowLog.gridx = 0;
		gbc_btnShowLog.gridy = 17;
		boundsPanel.add(showLogBtn, gbc_btnShowLog);

		JButton resetBtn = new JButton("Reset Board");
		GridBagConstraints gbc_resetBtn = new GridBagConstraints();
		gbc_resetBtn.fill = GridBagConstraints.BOTH;
		gbc_resetBtn.gridx = 0;
		gbc_resetBtn.gridy = 18;
		resetBtn.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				resetBoard();
				
			}
		});
		
		boundsPanel.add(resetBtn, gbc_resetBtn);
		
		boundsPanel.setPreferredSize(new Dimension(boundsPanel.getPreferredSize().width, 600));
		
	}
	
	public JPanel createWidthPanel(){
		JPanel xPanel = new JPanel();

		xPanel.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), 
					"Width", TitledBorder.TRAILING, TitledBorder.TOP, null, new Color(0, 70, 213)));
		JToggleButton x2Btn = new JToggleButton("2");
		x2Btn.addActionListener(dimensionListener);
		x2Btn.setActionCommand("2");
		xBtnGrp.add(x2Btn);
		xPanel.add(x2Btn);
		
		JToggleButton x3Btn = new JToggleButton("3");
		x3Btn.addActionListener(dimensionListener);
		x3Btn.setActionCommand("3");
		xBtnGrp.add(x3Btn);
		xPanel.add(x3Btn);
		
		JToggleButton x4Btn = new JToggleButton("4");
		x4Btn.addActionListener(dimensionListener);
		x4Btn.setActionCommand("4");
		xBtnGrp.add(x4Btn);
		xPanel.add(x4Btn);
		
		return xPanel;
	}
	
	public JPanel createHeightPanel(){
		JPanel yPanel = new JPanel();
		
		yPanel.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), 
										  "Height", TitledBorder.TRAILING, TitledBorder.TOP, 
										  null, new Color(0, 70, 213)));
		JToggleButton y2Btn = new JToggleButton("2");
		y2Btn.addActionListener(dimensionListener);
		y2Btn.setActionCommand("2");
		yBtnGrp.add(y2Btn);
		yPanel.add(y2Btn);
		
		JToggleButton y3Btn = new JToggleButton("3");
		y3Btn.addActionListener(dimensionListener);
		y3Btn.setActionCommand("3");
		yBtnGrp.add(y3Btn);
		yPanel.add(y3Btn);
		
		JToggleButton y4Btn = new JToggleButton("4");
		y4Btn.addActionListener(dimensionListener);
		y4Btn.setActionCommand("4");
		yBtnGrp.add(y4Btn);
		yPanel.add(y4Btn);
		
		return yPanel;
		
	}
	
	public JPanel createRoomsPanel(int x, int y){
		JPanel roomsPanel = new JPanel();

		roomsPanel.setLayout(new GridLayout(y, x, 0, 0));
		RoomLayout roomLayout = RoomArranger.getRoomLayout(x, y);
		roomPanels = new RoomPanel[x][y];
		for(int j=0; j<y; j++){
			for(int i=0; i<x; i++){
				Set<Evidence>[] evidence = roomLayout.getEvidence(i, j);
				roomPanels[i][j] = new RoomPanel(i, j, evidence[0], evidence[1]);
				roomsPanel.add(roomPanels[i][j]);
			}
		}
		
		return roomsPanel;
	}
	
	private class DimensionListener implements ActionListener{

		public void actionPerformed(ActionEvent e) {
			ButtonModel xSel = xBtnGrp.getSelection();
			ButtonModel ySel = yBtnGrp.getSelection();
			if(xSel == null || ySel == null){
				// Selection is not yet complete
				return;
			}
			
			int x = Integer.parseInt(xSel.getActionCommand());
			int y = Integer.parseInt(ySel.getActionCommand());
			
			updateDimensions(x, y);
		}
	}
	
	public void updateDimensions(int x, int y){
		boardDimension.setSize(x, y);
		JPanel roomsPanel = createRoomsPanel(x, y);
		roomWrapper.removeAll();
		roomWrapper.add(roomsPanel);
		roomWrapper.updateUI();
	}
	
	public void displaySolvedBoard(Room[][] solvedRoom){
		if(isSolved(solvedRoom)){
			// TODO: Hook for displaying a dialog that a complete solution wasnt possible
		}
		
		for(int i=0; i<roomPanels.length; i++){
			for(int j=0; j<roomPanels[i].length; j++){
				Room room = solvedRoom[i][j];
				roomPanels[i][j].setSelectedEvidence(room.getPersonSet(), room.getItemSet());
			}
		}
	}
	
	public boolean isSolved(Room[][] rooms){
		for(int i=0; i<rooms.length; i++){
			for(int j=0; j<rooms[i].length; j++){
				if(!isSolved(rooms[i][j])){
					return false;
				}
			}
		}
		
		return true;
	}
	
	public boolean isSolved(Room room){
		if(room.getItemSet().size() > 1){
			return false;
		}
		if(room.getPersonSet().size() > 1){
			return false;
		}
		
		return true;
	}
	
	private Room[][] solveCrime(){
		Room[][] rooms = createRooms();
		List<Clue> clues = createClues();
		
		Room[][] solvedRooms = RoomManager.solveCrime(rooms, clues);
		return solvedRooms;
	}
	
	private Room[][] createRooms(){
		Room[][] rooms = new Room[roomPanels.length][roomPanels[0].length];
		for(int i=0; i<rooms.length; i++){
			for(int j=0; j<rooms[i].length; j++){
				rooms[i][j] = new Room(i, j, roomPanels[i][j].getSelectedPeople(), 
											 roomPanels[i][j].getSelectedItems());
			}
		}
		
		return rooms;
	}
	
	private List<Clue> createClues(){
		return clueBuilderDialog.getClues();
	}

	public void resetBoard(){
		resetClues();
		resetRoomPanels();
	}
	
	private void resetClues(){
		clueBuilderDialog.resetClue();
		unsplitClueList = clueBuilderDialog.getUnsplitClues();
		clueListModel.removeAllElements();
	}
	
	private void resetRoomPanels(){
		roomWrapper.removeAll();
		roomWrapper.updateUI();
	}
}
